﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.W_14._10._20
{
    class Magician : IComparable<Magician>, INameable
    {
        public string Name { get; private set; }
        public string BirthTown { get; private set; }
        public string FavoriteSpell { get; private set; }
        internal string this[string data]
        {
            get
            {
                switch (data)
                {
                    case "Name":
                        return Name;
                    case "Birth Town":
                        return BirthTown;
                    case "Favorite Spell":
                        return FavoriteSpell;
                    default:
                        return "Unknown";
                }
            }
            set
            {
                switch (data)
                {
                    case "Name":
                        Name = value;
                        break;
                    case "Birth Town":
                        BirthTown = value;
                        break;
                    case "Favorite Spell":
                        FavoriteSpell = value;
                        break;
                }
            }
        }
        public override string ToString()
        {
            return $"Name: {Name}, Birth Town {BirthTown}, Favorite Spell {FavoriteSpell}";
        }
        int IComparable<Magician>.CompareTo(Magician other)
        {
            return Name.CompareTo(other.Name);
        }
    }
}
