﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.W_14._10._20
{
    interface INameable
    {
        string Name { get; }
    }
}
