﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.W_14._10._20
{
    class Knight : IComparable<Knight>, INameable
    {
        public string Name { get; private set; }
        public string BirthTown { get; private set; }
        public string Title { get; private set; }

        internal string this[string data]
        {
            get
            {
                switch (data)
                {
                    case "Name":
                        return Name;
                    case "Birth Town":
                        return BirthTown;
                    case "Title":
                        return Title;
                    default:
                        return "Unknown";
                }
            }
            set
            {
                switch (data)
                {
                    case "Name":
                        Name = value;
                        break;
                    case "Birth Town":
                        BirthTown = value;
                        break;
                    case "Title":
                        Title = value;
                        break;
                }
            }
        }
        public override string ToString()
        {
            return $"Name: {Name}, Birth Town {BirthTown}, Title {Title}";
        }
        public int CompareTo(Knight other)
        {
            return Name.CompareTo(other.Name);
        }
    }
}
