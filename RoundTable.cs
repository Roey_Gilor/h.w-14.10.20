﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.W_14._10._20
{
    class RoundTable<T> : IEnumerable<T> where T: IComparable<T>, INameable
    {
        private List<T> entities = new List<T>();
        public void Add(T entity)
        {
            entities.Add(entity);
        }
        public void RemoveAT(int index)
        {
            if (entities.Count == 0)
                return;
            if (entities.Count >= index)
                entities.RemoveAt(index - 1);
            else
                entities.RemoveAt(index - 1 - entities.Count);
        }
        public void InsertAt(int index, T entity)
        {
            if (entities.Count >= index)
                entities.Insert(index - 1, entity);
            else
                entities.Insert(index - 1 - entities.Count, entity);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return entities.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return entities.GetEnumerator();
        }
        public void Sort()
        {
            entities.Sort();
        }
        public void Clear()
        {
            entities.Clear();
        }
        public List<T> GetRounded(int number)
        {
            List<T> list = new List<T>();
            int count = 1;
            if (entities.Count >= number)
            {
                while (count <= number)
                {
                    entities.ForEach(item => list.Add(item));
                    count++;
                }
            }
            else
            {
                number -= entities.Count;
                entities.ForEach(item => list.Add(item));
                while (count <= number)
                {
                    entities.ForEach(item => list.Add(item));
                    count++;
                }
            }
            return list;
        }
        public T this[int index]
        {
            get
            {
                if (entities.Count == 0)
                    return default;
                if (entities.Count >= index)
                    index -= 1 - entities.Count;
                return entities[index];
            }
        }
        public T this[string name]
        {
            get
            {
                if (entities.Count == 0)
                    return default;
                foreach (T item in entities)
                {
                    if (item.Name == name)
                        return item;
                }
                return default;
            }
        }
    }
}
